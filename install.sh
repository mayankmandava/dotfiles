for file in $( ls -a | grep '^\.[^.]'| grep -v git ); do 
	rm ~/$file
	ln -s ~/dotfiles/$file ~/$file
	echo $file linked
done


mkdir -p ~/.vim/bundle
mkdir -p ~/.tmux/pugins
git clone https://github.com/gmarik/Vundle.vim.git ~/.vim/bundle/Vundle.vim
git clone https://github.com/tmux-plugins/tpm ~/.tmux/plugins/tpm
