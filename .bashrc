PATH="/Users/mandu/Library/Android/sdk/platform-tools:$PATH"
PATH="$HOME/local/bin:/usr/local/bin:$PATH"
PATH="/Applications/Postgres.app/Contents/Versions/9.3/bin:$PATH"

M2_HOME=/usr/local/Cellar/maven/3.3.3
M2=$M2_HOME/bin  
PATH=$M2:$PATH  

if [ -f ~/.bash_prompt ]; then
	    . ~/.bash_prompt
fi

if [ -f /opt/intel/composer_xe_2015/mkl/bin/mklvars.sh ]; then
   source /opt/intel/composer_xe_2015/mkl/bin/mklvars.sh intel64
fi

if [ -f /usr/local/etc/bash_completion.d/password-store ]; then
   . /usr/local/etc/bash_completion.d/password-store
fi

#eval $(gpg-agent --daemon)

# The following line is for mosh. Add to server also
export LC_CTYPE="en_US.UTF-8"

fixssh() {
  for key in SSH_AUTH_SOCK SSH_CONNECTION SSH_CLIENT; do
    if (tmux show-environment | grep "^${key}" > /dev/null); then
      value=`tmux show-environment | grep "^${key}" | sed -e "s/^[A-Z_]*=//"`
      export ${key}="${value}"
    fi
  done
}

alias a='. env/bin/activate'
alias d='deactivate'

# added by newengsetup
export EDITOR=vim
export UBER_HOME="$HOME/Uber"
export UBER_OWNER="mandava@uber.com"
export VAGRANT_DEFAULT_PROVIDER=aws
[ -s "/usr/local/bin/virtualenvwrapper.sh" ] && . /usr/local/bin/virtualenvwrapper.sh
[ -s "$HOME/.nvm/nvm.sh" ] && . $HOME/.nvm/nvm.sh
type "brew" &>/dev/null && [ -s "$(brew --prefix)/etc/bash_completion" ] && . $(brew --prefix)/etc/bash_completion

cdsync () {
    cd $(boxer sync_dir $@)
}
editsync () {
    $EDITOR $(boxer sync_dir $@)
}
opensync () {
    open $(boxer sync_dir $@)
}
